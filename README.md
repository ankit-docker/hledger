# Hledger

## Usage

Note: This repository uses [Podman](https://podman.io).

Run the Podman container with the Hledger docker image and mount Ledger file.

```bash
$ podman run \
  --detach \
  --name hledger \
  --volume $DIRECTORY:/ledger:Z \
  --port 30002:80 \
  --environment LEDGER_FILE=/ledger/ankit.journal \
  --environment BASE_URL=https://ledger.argd.in \
  --restart always \
  registry.gitlab.com/ankit-docker/hledger/hledger:latest
```
