# Dockerfile for Miniflux
#
# Copyright 2020, Ankit R Gadiya
# BSD 3-Clause License
FROM debian:buster-slim
LABEL MAINTAINER="Ankit R Gadiya git@argp.in"

RUN apt-get update && \
  apt-get install hledger-web -y

COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /ledger

EXPOSE 80
ENTRYPOINT ["/entrypoint.sh"]
