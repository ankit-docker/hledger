#!/bin/bash

/usr/bin/hledger-web \
    --server \
    --host 0.0.0.0 \
    --port 80 \
    --base-url "${BASE_URL}" \
    --capabilities=view
